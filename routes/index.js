const express = require('express');
const router = express.Router();

router.get('/', (req, res) => res.json({status: 'NodeJS backend'}));

module.exports = router;